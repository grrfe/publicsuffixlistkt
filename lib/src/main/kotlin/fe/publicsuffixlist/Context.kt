package fe.publicsuffixlist

import mozilla.components.lib.publicsuffixlist.PublicSuffixListLoader
import java.io.InputStream

object Context {
    val assets = Resources
}

object Resources {
    fun open(name: String): InputStream {
        return PublicSuffixListLoader::class.java.getResourceAsStream("/$name")!!
    }
}
